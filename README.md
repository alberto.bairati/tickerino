# Tickerino

> A display showing news from the web

*Tickerino* is a small LCD display controlled by an [Arduino UNO](https://www.arduino.cc/en/main/arduinoBoardUno) compatible board that parses an XML file created by a remote PHP script. The script retrives and aggregates info and news from some RSS feed, leaving to the board the only task to show and cycle them on the display.

## Hardware setup

### Components

- [Wemos D1 R2](https://www.wemos.cc/product/d1.html)
- [LCD 16x2 (Hitachi HD44780 driver)](https://www.arduino.cc/en/Tutorial/HelloWorld)
- [Controller I2C PCF8574T](http://www.adrirobot.it/display_lcd/lcd_seriale_i2c/scheda_display_lcd_i2c.htm)

### Connections

The I2C controller pins should be connected to the board in this way:

- SDA to *D2/SDA*
- SCL to *D1/SCL*
- VCC to *+5V*
- GND to *GND*

## Software setup

1. Rename `config-sample.php` to `config.php`.
2. Get an [OpenWeatherMap API key](https://openweathermap.org/appid).
3. In `config.php`, assign your OpenWeatherMap API key to the variable `$openWeatherMapApi`.
4. Upload `api.php` and `config.php` to a remote server.
5. Open `firmware/firmware.ino`, add your WiFi SSID, password, server and path to the remote `api.php`.
5. Uplaod the scketch to your board.

## Resources

### Getting started with the Wemos D1 on Mac

- [This post](https://blog.rjdlee.com/getting-started-with-wemos-d1-on-mac-osx/) is very helpful for the board setup in a OSX environment.
- The drivers for Mac are available in [this page](https://blog.sengotta.net/signed-mac-os-driver-for-winchiphead-ch340-serial-bridge/).

### Development notes

- The LCD control by I2C has been implemented starting from [this post](https://forum.arduino.cc/index.php?topic=128635.0)
- The parsing of the XML file uses [TextFinder library](http://playground.arduino.cc/Code/TextFinder)

### News sources

The news are retrieved from these feeds:

- [OpenWheatherMap](https://openweathermap.org/api)
- [Ultima Ora di ANSA.it](http://www.ansa.it/sito/notizie/topnews/topnews_rss.xml)
- [News RAI Televideo](http://www.televideo.rai.it/televideo/pub/rss101.xml)
- [Repubblica.it Homepage](http://www.repubblica.it/rss/homepage/rss2.0.xml)
- [Reuters World News](http://feeds.reuters.com/Reuters/worldNews)