<?php

// TICKERINO API CONFIGURATION FILE

// Show the current date/time?
$showDateTime = true;

// How many news for each source?
$nAnsa = 0;
$nRai = 5;
$nRepubblica = 0;
$nReuters = 0;

// Show the sorce for every news?
$showSource = false;

// Weather setup
$weatherNow = true;
$weatherTomorrow = true;
$openWeatherDefaultCity = 'Milano';
$openWeatherCountry = 'IT';
$openWeatherMapApi = '<YOUR-OPENWEATHERMAP-API-KEY>';

// Caching setup
$secondsCachingTime = 60;
$cacheFileName = 'tickerino.xml';

// Other config
$curlUserAgent = 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36';
$maxCharLine1 = 40;
$maxCharLine2 = 40;

?>