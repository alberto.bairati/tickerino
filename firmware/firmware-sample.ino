/*
|
|   TICKERINO
|   Wemos D1 R2
|   -----------
|   I2C connections:
|   - SDA  ->   D2/SDA
|   - SCL  ->   D1/SCL
|   - VCC  ->   +5V
|   - GND  ->   GND
|
*/

#include <Wire.h>
#include <SPI.h>
#include <LiquidCrystal_I2C.h>
#include <ESP8266WiFi.h>
#include <TextFinder.h>

LiquidCrystal_I2C lcd(0x3F, 16, 2);
WiFiClient client;
TextFinder finder(client);

const char* ssid = "<YOUR-WIFI-SSID>";
const char* password = "<YOUR-WIFI-PASSWORD>";

byte server[] = { 000, 000, 00, 000 };              		  // The IP address of your site
String host = "your.site.com";                 			      // Server address in letter
String path = "/projects/tickerino/api.php?city=Rome";        // The path to the api.php script

void setup()
{
    Serial.begin(9600);

    // Setup LCD
    lcd.begin(16,2);
    lcd.init();
    lcd.backlight();

    // Splash screen
    lcd.clear();
    lcd.setCursor(0,1);
    lcd.print("Tickerino");

    delay(500);
}

void loop()
{

    Serial.println("Checking connection");

    // If I'm not connected to the WiFi, I connect

    if(WiFi.status() != WL_CONNECTED){

        WiFi.begin(ssid, password);

        Serial.println("Connecting to");
        Serial.print(ssid);

        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Connecting to");
        lcd.setCursor(0,1);
        lcd.print(ssid);

        while (WiFi.status() != WL_CONNECTED) {
            delay(500);
            Serial.print(".");
        }

        // When I'm connected, show the RSSI (Received Signal Strenght Indication)

        long rssi = WiFi.RSSI();

        Serial.println(" connected");
        Serial.print("RSSI: ");
        Serial.print(rssi);
        Serial.println(" dbm");

        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print(ssid);
        lcd.setCursor(0,1);
        lcd.print("RSSI: ");
        lcd.print(rssi);
        lcd.print(" dBm");

        delay(1000);

    }else{

        // Already connected to the WiFi
        Serial.println("Already connected");
    }

    // Connecting to the server

    if (client.connect(server, 80)) {

        String callPath = "GET " + path + " HTTP/1.1";
        client.println(callPath);
        String callHost = "Host: " + host;
        client.println(callHost);
        client.println();

        if (client.connected()) {

            Serial.println("Connected to server ");
            Serial.println("---");

            if (finder.find("<tickerino")){ 

                // How many items to cycle through?
                finder.find("<news items="); 
                int nNews = finder.getValue();

                // Proceed item by item
                for(int i=1; i<=nNews; i++){

                    // Look for the item node
                    String stringMarker = "<item id=\"" + String(i) + "\">";
                    char charMarker[25];
                    stringMarker.toCharArray(charMarker, 25);
                    finder.find(charMarker);

                    // First line of the display
                    char line1[41];
                    if (finder.getString("<l1>", "</l1>", line1, 41)!=0) {
                        Serial.println(line1);
                    }

                    // Second line of the display
                    char line2[41];
                    if (finder.getString("<l2>", "</l2>", line2, 41)!=0) {
                        Serial.println(line2);
                    }

                    // Measure lines lenghts
                    int lengLine1 = strlen(line1);
                    int lengLine2 = strlen(line2);

                    lcd.clear();

                    // Print the first line
                    lcd.setCursor(0,0);
                    lcd.print(line1);

                    // Print the second line, handling the ° sign
                    lcd.setCursor(0,1);
                    String sLine2 = String(line2);
                    if(sLine2.indexOf('°')!=-1){
                        String p1 = getValue(sLine2,'°',0);
                        String p2 = getValue(sLine2,'°',1);
                        lcd.print(p1.toInt());
                        lcd.print((char)223);
                        lcd.print(p2);
                    }else{
                        lcd.print(line2);
                    }

                    delay(2000);

                    // If necessary, scroll the display
                    int lengLongerLine = lengLine2;
                    if(lengLine1 > lengLine2) lengLongerLine = lengLine1;
                    if(lengLongerLine > 16){
                        for (int pCounter = 0; pCounter < lengLongerLine-16; pCounter++) {
                            lcd.scrollDisplayLeft();
                            delay(350);
                        }
                        delay(1000);
                    }else{
                        delay(3000);   
                    }

                    lcd.clear();
                    Serial.println("---");
                    delay(500);
                }
            }else{

                // If I've a problem with the server output

                Serial.println("XML error");

                lcd.clear();
                lcd.setCursor(0,1);
                lcd.print("XML error");

                delay(500);
            }

        }

    } else {

        // Can't connect to the server

        Serial.println("No response");

        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("No response");
        lcd.setCursor(0,1);
        lcd.print("from server");

        delay(10000);
    }
}

// http://stackoverflow.com/questions/9072320/split-string-into-string-array

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = {0, -1};
    int maxIndex = data.length()-1;

    for(int i=0; i<=maxIndex && found<=index; i++){
        if(data.charAt(i)==separator || i==maxIndex){
            found++;
            strIndex[0] = strIndex[1]+1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }

    return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}
