<?php

require 'config.php';

// Look for the city

if(isset($_GET['city']) && $_GET['city']!=''){
    $openWeatherCity = $_GET['city'];
}else{
    $openWeatherCity = $openWeatherDefaultCity;
}

// Serve the XML

header('Content-Type: text/xml; charset=utf-8');

if(file_exists($cacheFileName)){

    // If the XML file exists, verify its creation date
    $nowUnix = date_timestamp_get(date_create());
    $fileCreatedUnix = date('U', date(filemtime($cacheFileName)));
    if(($nowUnix - $fileCreatedUnix) > $secondsCachingTime ){

        // It's too old: rewrite it!
        $output = writeCacheFile();
        echo $output;
    }else{

        // It's recent: read it!
        $file = fopen($cacheFileName, "r") or die("Unable to open file!");
        echo fread($file,filesize($cacheFileName));
        fclose($file);
    }
}else{

    // It doesn't exist: write it!
    $output = writeCacheFile();
    echo $output;
}

// Writing the cache file

function writeCacheFile(){

    global $showDateTime, $nAnsa, $nRai, $nRepubblica, $nReuters, $weatherNow, $weatherTomorrow, $openWeatherCity, $openWeatherCountry, $openWeatherMapApi, $curlUserAgent, $cacheFileName, $maxCharLine1, $maxCharLine2, $lineSeparator, $showSource;
    $openWeatherCity = str_replace(' ', '%20', $openWeatherCity);
    $tickerino = array();
    $c = 1;
    
    // Date and time info
    
    if($showDateTime){

        $tickerino['news'][$c]['l1'] = "Tickerino";
        $tickerino['news'][$c]['l2'] = date("d.m.y").", ".date("H:i");
        $c++;
    }

    // Read current weather

    if($weatherNow){

        $url = "http://api.openweathermap.org/data/2.5/weather?q=$openWeatherCity,$openWeatherCountry&appid=$openWeatherMapApi&mode=xml&units=metric&lang=it";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $curlUserAgent);

        $feed = curl_exec($ch);
        $xml = new SimpleXMLElement($feed);

        $luogo = $xml->city['name'];
        $condizioni = $xml->weather['value'];
        $temperatura = round($xml->temperature['value']).'°C';
        $tickerino['news'][$c]['l1'] = cleanString('Adesso a '.$luogo.':');
        $tickerino['news'][$c]['l2'] = cleanString($temperatura.', '.$condizioni);
        $c++;
    }

    // Read ANSA news

    if($nAnsa > 0){ 

        $url = 'http://www.ansa.it/sito/notizie/topnews/topnews_rss.xml';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $curlUserAgent);

        $feed = curl_exec($ch);
        $xml = new SimpleXMLElement($feed);

        for($i=0; $i<=$nAnsa-1; $i++){
            $ora = strtotime($xml->channel->item[$i]->pubDate);
            $ora = date('H:i', $ora);
            $l1 = $ora;
            if($showSource) $l1 = $ora.' - Ansa';
            $tickerino['news'][$c]['l1'] = $l1;
            $tickerino['news'][$c]['l2'] = cleanString($xml->channel->item[$i]->title);
            $c++;
        }
    }
    
    // Read RAI news

    if($nRai > 0){ 

        $url = 'http://www.televideo.rai.it/televideo/pub/rss101.xml';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $curlUserAgent);

        $feed = curl_exec($ch);
        $xml = new SimpleXMLElement($feed);

        for($i=0; $i<=$nRai-1; $i++){
            $ora = strtotime($xml->channel->item[$i]->pubDate);
            $ora = date('H:i', $ora);
            $l1 = $ora;
            if($showSource) $l1 = $ora.' - Rai';
            $tickerino['news'][$c]['l1'] = $l1;
            $tickerino['news'][$c]['l2'] = cleanString($xml->channel->item[$i]->title);
            $c++;
        }
    }

    // Read Repubblica news

    if($nRepubblica > 0){

        $url = 'http://www.repubblica.it/rss/homepage/rss2.0.xml';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $curlUserAgent);

        $feed = curl_exec($ch);
        $xml = new SimpleXMLElement($feed);

        for($i=0; $i<=$nRepubblica-1; $i++){
            $ora = strtotime($xml->channel->item[$i]->pubDate);
            $ora = date('H:i', $ora);
            $l1 = $ora;
            if($showSource) $l1 = $ora.' - Repubblica';
            $tickerino['news'][$c]['l1'] = $l1;
            $tickerino['news'][$c]['l2'] = cleanString($xml->channel->item[$i]->title);
            $c++;
        }
    }

    // Read Reuters news

    if($nReuters > 0){

        $url = 'http://feeds.reuters.com/Reuters/worldNews';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $curlUserAgent);

        $feed = curl_exec($ch);
        $xml = new SimpleXMLElement($feed);

        for($i=0; $i<=$nReuters-1; $i++){
            $ora = strtotime($xml->channel->item[$i]->pubDate);
            $ora = date('H:i', $ora);
            $l1 = $ora;
            if($showSource) $l1 = $ora.' - Reuters';
            $tickerino['news'][$c]['l1'] = $l1;
            $tickerino['news'][$c]['l2'] = cleanString($xml->channel->item[$i]->title);
            $c++;
        }
    }

    // Read tomorrow weather

    if($weatherTomorrow){

        $url = "http://api.openweathermap.org/data/2.5/forecast/daily?cnt=2&q=$openWeatherCity,$openWeatherCountry&appid=$openWeatherMapApi&mode=xml&units=metric&lang=it";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $curlUserAgent);

        $feed = curl_exec($ch);
        $xml = new SimpleXMLElement($feed);

        $luogo = $xml->location->name;
        $condizioni = $xml->forecast->time[1]->symbol['name'];
        $temperatura = round($xml->forecast->time[1]->temperature['day']).'°C';
        $tickerino['news'][$c]['l1'] = cleanString('Domani a '.$luogo.':');
        $tickerino['news'][$c]['l2'] = cleanString($temperatura.', '.$condizioni);
        $c++;
    }

    // Format XML file

    $output = null;
    $items = $c-1;
    for($i = 1; $i <= $items; $i++){

        $line1 = '<l1>'.cutString($tickerino['news'][$i]['l1'], $maxCharLine1).'</l1>';
        $line2 = '<l2>'.cutString($tickerino['news'][$i]['l2'], $maxCharLine2).'</l2>';

        $output = $output.'<item id="'.$i.'">'.$line1.$line2.'</item>';
    }
    $output = '<news items="'.$items.'">'.$output.'</news>';
    $output = '<tickerino generated="'.date("Y-m-d H:i:s").'">'.$output.'</tickerino>';
    $output = '<?xml version="1.0" encoding="UTF-8"?>'.$output;

    // Save the file

    $fp = fopen($cacheFileName, 'w');
    fwrite($fp, $output);
    fclose($fp);

    return $output;

}

function cutString($stringa, $maxChar){

    if(strlen($stringa)>$maxChar){

        $stringa_tagliata=substr($stringa, 0,$maxChar);
        $last_space=strrpos($stringa_tagliata," ");
        $stringa_ok=substr($stringa_tagliata, 0,$last_space);
        return $stringa_ok."...";

    }else{

        return $stringa;
    }
}

function cleanString($stringa){

    $stringa = str_replace("á","a'",$stringa);
    $stringa = str_replace("à","a'",$stringa);
    $stringa = str_replace("é","e'",$stringa);
    $stringa = str_replace("è","e'",$stringa);
    $stringa = str_replace("É","E'",$stringa);
    $stringa = str_replace("È","E'",$stringa);
    $stringa = str_replace("ì","i'",$stringa);
    $stringa = str_replace("ò","o'",$stringa);
    $stringa = str_replace("ú","u'",$stringa);
    $stringa = str_replace("ù","u'",$stringa);

    return $stringa;
}

?>